# Split BCH and BSV

Because there is no replay protection added to BSV, you need to split your coins before you use them. Otherwise you risk loosing them on the other chain.

The general process:
  * Import wallet into electron-cash
  * Receive dust or sign with Schnorr
  * Combine all your coins with the dust and send it to a new address you control
  * Wait for a confirmation
  * Import wallet into electrum-sv
  * Combine all coins and send to a new address, no dust needed this time
  * Wait for a confirmation
  * Done!


## 1. Download both Electon-Cash and Electrum-SV

Always install the newest version of the wallet software.


[https://electroncash.org/](https://electroncash.org/)

[https://electrumsv.io/](https://electrumsv.io/)


## 2. Import your wallet into Electron Cash

Possible derivation paths:
* m/0'
* m/44'/0'/0'
* m/44'/145'/0'

Wallet types:

* Seedphrase
    * File > New/Restore
    * Name it & click next
    * Select standard wallet
    * Select I already have a seed
    * Enter your seed
    * Click the options button, select BIP39 seed
    * Enter your derivation path
    * Continue
* Paper wallet  
    * File > New/Restore
    * Name it & click next
    * Select Import Bitcoin Cash addresses or private keys
    * Enter your private key
    * Continue
* Hardware wallet
    * File > New/Restore
    * Name it & click next
    * Select standard wallet
    * Use Hardware wallet
    * Select your device
    * Enter your derivation path

## 3. Combine your coins with some BCH dust or sign with Schnorr

For **paper wallets and seed phrases** you can simply create a new transaction, send all your coins to yourself in a transaction that is signed with Schnorr.

Enable Schnorr (default since EC v4.0.3):

* Tools > Preferences > Transactions tab
* Select Sign with Schnorr signatures


Create transaction:

* Go to Recieve tab and copy the receiving address (for paper wallets, create or use another standard wallet for a new receiving address)
* Go to send tab and paste the receiving address
* Click the Max button (make sure the dust is included)
* Click send
* Done!


For **hardware wallets** you'll need to use some BCH dust, since most do not support Schnorr signatures:

* Get yourself some dust on a new receiving address
* Get a new receiving address
* Go to send tab
* Paste new receiving address
* Click the Max button (make sure the dust is included)
* Hit Send
* Done!

## 4. Wait for a confirmation 

## 5. Import your wallet into Electrum SV

Follow the same procedure as #2

## 6. Send all your coins to yourself on a new address

No need for dust this time.

## 7. Wait for a confirmation

## 8. Done!