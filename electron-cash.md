# CLI

## General

### Total amount of transactions in wallet

```>> len(wallet.get_history())```

```>> window.history_list.topLevelItemCount()```


### Total amount of utxos (coins) in wallet

```>> len(wallet.get_utxos())```


## Shuffle

### Total amount of shuffle transactions in wallet

```>> len([x[4] for x in wallet.get_history() if x[4] == -270])```

### Total fees paid on shuffle transactions

```>> sum([x[4] for x in wallet.get_history() if x[4] == -270])```


## Fusion

### Total amount of fusion transactions in wallet

```>> len([x[4] for x in wallet.get_history() if x[4] < 0 and x[4] > -10000])```

### Total fees paid on fusion transactions

```>> sum([x[4] for x in wallet.get_history() if x[4] < 0 and x[4] > -10000])```


# Run EC from source


for secp256k1

```
./contrib/make_secp
```

for zbar 

```
./contrib/make_zbar
```


for tor 

```
./contrib/make_openssl && ./contrib/make_libevent && ./contrib/make_zlib && ./contrib/make_tor
```

to clean up and make sure all submodules are on the right pointer: 

```
./contrib/make_clean
```





```
python -m venv env
source env/bin/activate
pip install -U -r contrib/requirements/requirements.txt
pip install -U -r contrib/requirements/requirements-binaries.txt
pip install -U -r contrib/requirements/requirements-hw.txt
```